$(document).ready(function(){
    $("#callback-btn").click(function(){
        $("#callback-block").toggleClass('open');
        $('body').css('overflow','hidden');
    });
    $('#header_phones>a').click(function(e){
        if(1200<=document.body.clientWidth){
            e.preventDefault();
            $("#callback-block").toggleClass('open');
            $('body').css('overflow','hidden');
        }
    });
    $("#callback-close").click(function(){
        $("#callback-block").removeClass('open');
        $('body').css('overflow','');
    });
    $("#callback-block").click(function(){
        $(this).removeClass('open');
        $('body').css('overflow','');
    }).on('click','.callback-wrap', function (e) {
        e.stopPropagation();
    });
    $('#rd-callback-form').submit(function (e) {
        e.preventDefault();
        var $form = $(this);
        $(this).children('.preloader').addClass('open');
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize()+'&ACTION=callback',
            success: function (data) {
                $('#rd-callback-form').html(data);
            }
        });
    });
});