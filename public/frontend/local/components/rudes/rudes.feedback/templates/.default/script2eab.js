$(document).ready(function(){
    $("[data-role=rd-feedback]").click(function(){
        $(".feedback-hidden-block").addClass('open');
        $(".feedback-bg").addClass('open');
        $('body').css('overflow','hidden');
        $('#feedback-form-source').val($(this).attr('data-formtype'));
    });
    $(".feedback-close").click(function(){
        $(this).closest('.feedback-hidden-block').removeClass('open');
        $(this).closest(".feedback-bg").removeClass('open');
        $('body').css('overflow','');
    });
    $(".feedback-bg").click(function(){
        $(this).children('.feedback-hidden-block').removeClass('open');
        $(this).removeClass('open');
        $('body').css('overflow','');
    }).on('click','.feedback-hidden-block', function (e) {
        e.stopPropagation();
    });
    $('.rd-feedback-form').submit(function (e) {
        e.preventDefault();
        var form = $(this);
        $(this).children('.preloader').addClass('open');
        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize()+'&ACTION=feedback',
            success: function (data) {
                form.html(data);
            }
        });
    });
    $('.rd-feedback-form label').click(function () {
        var forItem=$(this).attr('for');
        $('#'+forItem).trigger('focus');
    });
    $('.rd-feedback-form .input-item').focusin(function () {
        $(this).closest('.form-field').addClass('focused');
        if(''!=$(this).val()){
            if(!$(this).closest('.form-field').hasClass('filled')){
                $(this).closest('.form-field').addClass('filled');
            }
        }else{
            if($(this).closest('.form-field').hasClass('filled')){
                $(this).closest('.form-field').removeClass('filled');
            }
        }
    });
    $('.rd-feedback-form .input-item').focusout(function () {
        $(this).closest('.form-field').removeClass('focused');
        if(''!=$(this).val()){
            if(!$(this).closest('.form-field').hasClass('filled')){
                $(this).closest('.form-field').addClass('filled');
            }
        }else{
            if($(this).closest('.form-field').hasClass('filled')){
                $(this).closest('.form-field').removeClass('filled');
            }
        }
    });
});