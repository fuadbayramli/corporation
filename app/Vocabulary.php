<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vocabulary extends Model
{
    protected $fillable = [
        'alphabet_id',
        'title',
        'short',
        'order_index',
    ];

    public function alphabet(){
        return $this->belongsTo('App\Alphabet');
    }
}
