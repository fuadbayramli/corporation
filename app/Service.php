<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'short',
        'price',
        'category_id',
        'img',
    ];

    public function category(){
        return $this->belongsTo('App\ServiceCategory');
    }
}
