<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteInformation extends Model
{
    protected $fillable = [
        'name',
        'main_email',
        'main_number',
        'address',
        'link',
        'logo',
        'footer_logo',
        'correspond_address',
        'short',
        'description',
        'status',
    ];
}
