<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarMake extends Model
{
    protected $fillable = [
        'name',
        'car_type_id',
    ];

    public function car_type(){
        return $this->belongsTo('App\CarType');
    }

    public function car_models(){
        return $this->hasMany('App\CarModel');
    }
}
