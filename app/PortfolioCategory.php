<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PortfolioCategory extends Model
{
    protected $fillable = [
        'name',
        'slug',
    ];

    public function portfolios(){
       return $this->hasMany('App\Portfolio');
    }
}
