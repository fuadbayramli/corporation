<?php

namespace App\Providers;

use App\Article;
use App\News;
use App\OurClient;
use App\Portfolio;
use App\PortfolioCategory;
use App\Service;
use App\ServiceCategory;
use App\SiteInformation;
use App\User;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        View::share('base_users', User::all());
        View::share('base_news', News::all());
        View::share('base_articles', Article::all());
        View::share('base_clients', OurClient::all());
        View::share('base_portfolios', Portfolio::all());
        View::share('base_services', Service::all());
        View::share('base_port_cats', PortfolioCategory::all());
        View::share('base_serv_cats', ServiceCategory::all());

        View::share('site_information', SiteInformation::where("status","Active")->first());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
