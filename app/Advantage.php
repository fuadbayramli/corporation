<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advantage extends Model
{
    protected $fillable = [
        'img',
        'name',
        'title',
        'slug',
        'short',
    ];
}
