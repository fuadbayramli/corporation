<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarModel extends Model
{
    protected $fillable = [
        'name',
        'car_make_id',
    ];

    public function car_make(){
        return $this->belongsTo('App\CarMake');
    }


}
