<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class AdminUserController extends Controller
{


    public function user_access($page_slug){
        $return_val = false;
        if(Auth::check()){
            $permissions = array();
            foreach(Auth::user()->role->role_items()->pluck('slug')->toArray() as $key => $rol_item){
                array_push($permissions,$rol_item);
            }
            if (in_array($page_slug,$permissions)){
                $return_val = true;
            }
        }
        if(!$return_val){
            return abort(403);
        }
        //return $return_val;
    }




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->user_access('users-manage');

        $search_value = $request->get('q', '');
        $users = User::query()
            ->where('name', 'LIKE', "%$search_value%")
            ->orWhere('email', 'LIKE', "%$search_value%")
            ->orderBy('created_at', 'desc')
            ->paginate(50);

        return view('backend.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->user_access('users-manage');

        $roles = Role::pluck('name','id')->all();

        return view('backend.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = Input::file('img');
        $filename = time() . '.' . $image->getClientOriginalExtension();
        $path = public_path('images/' . $filename);

        Image::make($image->getRealPath())->resize(790, 594)->save($path);

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->img = $filename;
        $user->role_id = $request->role_id;
        $user->save();

        Auth::login($user);

        return redirect('admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->user_access('users-manage');

        $user = User::findOrFail($id);

        $roles = Role::pluck('name', 'id')->all();

        return view('backend.users.edit', compact('roles','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = Input::file('img');

        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);

        $user->role()->associate($request->role_id);

        if ($image != null) {

            $file= $user->img;
            $old_file_name = public_path().'/images/'.$file;
            File::delete($old_file_name);

            $filename = time() . '.' . $image->getClientOriginalExtension();

            $path = public_path('images/' . $filename);
            Image::make($image->getRealPath())->resize(790, 594)->save($path);
            $user->img = $filename;
        }

        $user->save();

        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->user_access('users-manage');

        $user = User::findOrFail($id);
        $file= $user->img;
        $filename = public_path().'/images/'.$file;
        File::delete($filename);

        $user->delete();

        return redirect('/admin/users');
    }



    public function attendance($id)
    {
        $user = User::findOrFail($id);
        $this->authorize('modifyUser', $user);

        return view ('user.attendance', ['user'=>$user]);
    }
}
