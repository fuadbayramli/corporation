<?php

namespace App\Http\Controllers;

use App\Advantage;
use App\Http\Requests\AdvantageCreateRequest;
use App\Http\Requests\AdvantageEditRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class AdminAdvantageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search_value = $request->get('q', '');
        $advantages = Advantage::query()
            ->where('name', 'LIKE', "%$search_value%")
            ->where('title', 'LIKE', "%$search_value%")
            ->orWhere('slug', 'LIKE', "%$search_value%")
            ->orWhere('short', 'LIKE', "%$search_value%")
            ->orderBy('created_at', 'desc')
            ->paginate(50);
        return view('backend.advantages.index', compact( 'advantages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.advantages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdvantageCreateRequest $request)
    {
        $image = Input::file('img');
        $filename = time() . '.' . $image->getClientOriginalExtension();
        $path = public_path('images/' . $filename);

        Image::make($image->getRealPath())->save($path);

        $advantage = new Advantage;
        $advantage->img = $filename;
        $advantage->name = $request->name;
        $advantage->title = $request->title;
        $advantage->slug = $request->slug;
        $advantage->short = $request->short;

        $advantage->save();

        return redirect('admin/advantages');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $advantage = Advantage::findOrFail($id);

        return view('backend.advantages.edit',compact('advantage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdvantageEditRequest $request, $id)
    {
        $image = Input::file('img');

        $advantage = Advantage::find($id);
        $advantage->name = $request->name;
        $advantage->title = $request->title;
        $advantage->slug = $request->slug;
        $advantage->short = $request->short;

        if ($image != null) {

            $file= $advantage->img;
            $old_file_name = public_path().'/images/'.$file;
            File::delete($old_file_name);

            $filename = time() . '.' . $image->getClientOriginalExtension();

            $path = public_path('images/' . $filename);
            Image::make($image->getRealPath())->resize(310,277)->save($path);
            $advantage->img = $filename;
        }

        $advantage->save();

        return redirect('admin/advantages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advantage = Advantage::findOrFail($id);
        $file = $advantage->img;
        $filename = public_path().'/images/'.$file;
        File::delete($filename);

        $advantage->delete();

        return redirect('/admin/advantages');
    }
}
