<?php

namespace App\Http\Controllers;

use App\Http\Requests\OurClientCreateRequest;
use App\Http\Requests\OurClientEditRequest;
use App\OurClient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class AdminOurClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $our_clients = OurClient::paginate(50);
        return view('backend.our-clients.index', compact('our_clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.our-clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OurClientCreateRequest $request)
    {
        $image = Input::file('img');
        $filename = time() . '.' . $image->getClientOriginalExtension();
        $path = public_path('images/' . $filename);

        Image::make($image->getRealPath())->save($path);

        $our_client = new OurClient;
        $our_client->img = $filename;

        $our_client->save();

        return redirect('admin/our-clients');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $our_client = OurClient::findOrFail($id);
        return view('backend.our-clients.edit',compact('our_client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OurClientEditRequest $request, $id)
    {
        $image = Input::file('img');
        $our_client = OurClient::find($id);


        if ($image != null) {

            $file= $our_client->img;
            $old_file_name = public_path().'/images/'.$file;
            File::delete($old_file_name);

            $filename = time() . '.' . $image->getClientOriginalExtension();

            $path = public_path('images/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $our_client->img = $filename;
        }


        $our_client->save();

        return redirect('admin/our-clients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $our_client = OurClient::findOrFail($id);
        $file= $our_client->img;
        $filename = public_path().'/images/'.$file;
        File::delete($filename);

        $our_client->delete();

        return redirect('/admin/our-clients');
    }
}
