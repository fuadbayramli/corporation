<?php

namespace App\Http\Controllers;

use App\Http\Requests\SiteInformationCreateRequest;
use App\Http\Requests\SiteInformationEditRequest;
use App\SiteInformation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class AdminSiteInformationController extends Controller
{

    public function user_access($page_slug){
        $return_val = false;
        if(Auth::check()){
            $permissions = array();
            foreach(Auth::user()->role->role_items()->pluck('slug')->toArray() as $key => $rol_item){
                array_push($permissions,$rol_item);
            }
            if (in_array($page_slug,$permissions)){
                $return_val = true;
            }
        }
        if(!$return_val){
            return abort(403);
        }
        //return $return_val;
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->user_access('site-informations-manage');

        $search_value = $request->get('q', '');
        $site_informations = SiteInformation::query()
            ->where('name','LIKE', "%$search_value%")
            ->orWhere('main_email','LIKE', "%$search_value%")
            ->orWhere('main_number','LIKE', "%$search_value%")
            ->orWhere('address','LIKE', "%$search_value%")
            ->orWhere('link','LIKE', "%$search_value%")
            ->orderBy('created_at', 'desc')
            ->paginate(50);
        return view('backend.site-information.index', compact( 'site_informations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->user_access('site-informations-manage');

        return view('backend/site-information/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SiteInformationCreateRequest $request)
    {
        $logo = Input::file('logo');
        $filename = time() . '.' . $logo->getClientOriginalExtension();
        $path = public_path('images/' . $filename);

        Image::make($logo->getRealPath())->save($path);


        $footer_logo = Input::file('footer_logo');
        $filename2 = time() . '.' . $footer_logo->getClientOriginalExtension();
        $path2 = public_path('images/' . $filename2);

        Image::make($footer_logo->getRealPath())->save($path2);

        $site_information = new SiteInformation();
        $site_information->name = $request->name;
        $site_information->main_email = $request->main_email;
        $site_information->main_number = $request->main_number;
        $site_information->address = $request->address;
        $site_information->correspond_address = $request->correspond_address;
        $site_information->short = $request->short;
        $site_information->description = $request->description;
        $site_information->status = $request->status;
        $site_information->link = $request->link;
        $site_information->logo = $filename;
        $site_information->footer_logo = $filename2;

        $site_information->save();

        return redirect('admin/site-information');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->user_access('site-informations-manage');

        $site_information = SiteInformation::findOrFail($id);
        return view('backend.site-information.edit',compact('site_information'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SiteInformationEditRequest $request, $id)
    {
        $logo = Input::file('logo');
        $footer_logo = Input::file('footer_logo');

        $site_information = SiteInformation::find($id);
        $site_information->name = $request->name;
        $site_information->main_email = $request->main_email;
        $site_information->main_number = $request->main_number;
        $site_information->address = $request->address;
        $site_information->correspond_address = $request->correspond_address;
        $site_information->short = $request->short;
        $site_information->description = $request->description;
        $site_information->status = $request->status;
        $site_information->link = $request->link;

        if ($logo != null) {

            $file= $site_information->logo;
            $old_file_name = public_path().'/images/'.$file;
            File::delete($old_file_name);

            $filename = time() . '.' . $logo->getClientOriginalExtension();

            $path = public_path('images/' . $filename);
            Image::make($logo->getRealPath())->save($path);
            $site_information->logo = $filename;
        }
        if ($footer_logo != null) {

            $file2= $site_information->footer_logo;
            $old_file_name = public_path().'/images/'.$file2;
            File::delete($old_file_name);

            $filename2 = time() . '.' . $footer_logo->getClientOriginalExtension();

            $path2 = public_path('images/' . $filename2);
            Image::make($footer_logo->getRealPath())->save($path2);
            $site_information->footer_logo = $filename2;
        }

        $site_information->save();

        return redirect('admin/site-information');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->user_access('site-informations-manage');

        $site_information = SiteInformation::findOrFail($id);
        $file= $site_information->logo;
        $filename = public_path().'/images/'.$file;
        File::delete($filename);

        $file2= $site_information->footer_logo;
        $filename2 = public_path().'/images/'.$file2;
        File::delete($filename2);

        $site_information->delete();

        return redirect('/admin/site-information');
    }
}
