<?php

namespace App\Http\Controllers;

use App\Alphabet;
use App\Http\Requests\AlphabetCreateRequest;
use App\Http\Requests\AlphabetEditRequest;
use App\Vocabulary;
use Illuminate\Http\Request;

class AdminAlphabetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $search_value = $request->get('q', '');
        $alphabets = Alphabet::query()
            ->where('name', 'LIKE', "%$search_value%")
            ->orWhere('slug', 'LIKE', "%$search_value%")
            ->orWhere('lang', 'LIKE', "%$search_value%")
            ->orderBy('created_at', 'desc')
            ->paginate(50);

        return view('backend.alphabets.index', compact('alphabets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.alphabets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AlphabetCreateRequest $request)
    {
        $alphabet = new Alphabet;
        $alphabet->name = $request->name;
        $alphabet->slug = $request->slug;
        $alphabet->lang = $request->lang;

        $alphabet->save();

        $vocabulary_titles = $request->title;
        $order_indexes = $request->order_index;
        $vocabulary_shorts = $request->short;
        for ($i = 0; $i < count($vocabulary_titles); $i++) {
            if (!empty($vocabulary_titles[$i]) && !empty($vocabulary_shorts[$i])) {

                $vocabulary = new Vocabulary;
                $vocabulary->alphabet_id = $alphabet->id;
                $vocabulary->title = $vocabulary_titles[$i];
                $vocabulary->order_index = $order_indexes[$i];
                $vocabulary->short = $vocabulary_shorts[$i];
                $vocabulary->save();
            }
        }
        return redirect('admin/alphabets');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $alphabet = Alphabet::with('vocabularies')->where('id', $id)->first();

        $vocabularies = Vocabulary::query()->where('alphabet_id', $id)->orderBy('created_at', 'DESC')->get();

        return view('backend.alphabets.edit', compact('vocabularies', 'alphabet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AlphabetEditRequest $request, $id)
    {
        $alphabet = Alphabet::find($id);
        $alphabet->name = $request->name;
        $alphabet->slug = $request->slug;
        $alphabet->lang = $request->lang;

        $alphabet->save();

        $vocabulary_titles = $request->title;
        $order_indexes = $request->order_index;
        $vocabulary_shorts = $request->short;
        $post_data_array = array();
        for ($i = 0; $i < count($vocabulary_titles); $i++) {
            if (!empty($vocabulary_titles[$i]) && !empty($vocabulary_shorts[$i])) {
                $vocabulary = Vocabulary::firstOrNew(
                    array('alphabet_id' => $id,
                        'title' => $vocabulary_titles[$i],
                        'order_index' => $order_indexes[$i],
                        'short' => $vocabulary_shorts[$i],));
                $vocabulary->save();
                array_push($post_data_array, $vocabulary->id);
            }
        }
        Vocabulary::query()->where('alphabet_id', $id)->whereNotIn('id', $post_data_array)->delete();

        return redirect('admin/alphabets');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
