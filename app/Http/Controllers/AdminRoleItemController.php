<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleItemCreateRequest;
use App\Http\Requests\RoleItemEditRequest;
use App\RoleItem;
use Illuminate\Http\Request;

class AdminRoleItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search_value = $request->get('q', '');
        $role_items = RoleItem::query()
            ->where('name','LIKE', "%$search_value%")
            ->orWhere('slug','LIKE', "%$search_value%")
            ->orderBy('created_at', 'desc')
            ->paginate(50);
        return view('backend.role-items.index', compact( 'role_items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        return view('backend/role-items/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleItemCreateRequest $request)
    {
//        $role_item = new RoleItem;
//        $role_item->name = $request->name;
//        $role_item->slug = $request->slug;
//
//        $role_item->save();
//
//        return redirect('admin/role-items');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        $role_item = RoleItem::findOrFail($id);
//        return view('backend.role-items.edit',compact('role_item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleItemEditRequest $request, $id)
    {
//        $role_item = RoleItem::find($id);
//        $role_item->name = $request->name;
//        $role_item->slug = $request->slug;
//        $role_item->save();
//
//        return redirect('admin/role-items');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        $role_item = RoleItem::findOrFail($id);
//        $role_item->delete();
//
//        return redirect('/admin/role-items');
    }
}
