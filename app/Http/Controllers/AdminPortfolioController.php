<?php

namespace App\Http\Controllers;

use App\Http\Requests\PortfolioCreateRequest;
use App\Http\Requests\PortfolioEditRequest;
use App\Portfolio;
use App\PortfolioCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class AdminPortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request)
    {
        $search_value = $request->get('q', '');
        $portfolios = Portfolio::query()
            ->where('name', 'LIKE', "%$search_value%")
            ->where('title', 'LIKE', "%$search_value%")
            ->orWhere('slug', 'LIKE', "%$search_value%")
            ->orWhere('description', 'LIKE', "%$search_value%")
            ->orderBy('created_at', 'desc')
            ->paginate(50);
        return view('backend.portfolios.index', compact( 'portfolios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $portfolio_categories = PortfolioCategory::pluck('name', 'id')->all();

        return view('backend.portfolios.create', compact('portfolio_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PortfolioCreateRequest $request)
    {
        $image = Input::file('img');
        $filename = time() . '.' . $image->getClientOriginalExtension();
        $path = public_path('images/' . $filename);

        Image::make($image->getRealPath())->resize(310,277)->save($path);

        $portfolio = new Portfolio;
        $portfolio->img = $filename;
        $portfolio->name = $request->name;
        $portfolio->title = $request->title;
        $portfolio->slug = $request->slug;
        $portfolio->description = $request->description;
        $portfolio->category_id = $request->category_id;

        $portfolio->save();

        return redirect('admin/portfolios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $portfolio = Portfolio::findOrFail($id);
        $portfolio_categories = PortfolioCategory::pluck('name','id')->all();

        return view('backend.portfolios.edit',compact('portfolio', 'portfolio_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PortfolioEditRequest $request, $id)
    {
        $image = Input::file('img');

        $portfolio = Portfolio::find($id);
        $portfolio->name = $request->name;
        $portfolio->title = $request->title;
        $portfolio->slug = $request->slug;
        $portfolio->description = $request->description;
        $portfolio->category()->associate($request->category_id);

        if ($image != null) {

            $file= $portfolio->img;
            $old_file_name = public_path().'/images/'.$file;
            File::delete($old_file_name);

            $filename = time() . '.' . $image->getClientOriginalExtension();

            $path = public_path('images/' . $filename);
            Image::make($image->getRealPath())->resize(310,277)->save($path);
            $portfolio->img = $filename;
        }

        $portfolio->save();

        return redirect('admin/portfolios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $portfolio = Portfolio::findOrFail($id);
        $file = $portfolio->img;
        $filename = public_path().'/images/'.$file;
        File::delete($filename);

        $portfolio->delete();

        return redirect('/admin/portfolios');
    }
}
