<?php

namespace App\Http\Controllers;

use App\Http\Requests\OurTeamCreateRequest;
use App\Http\Requests\OurTeamEditRequest;
use App\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class AdminTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $our_teams = Team::paginate(50);
        return view('backend.our-teams.index', compact('our_teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.our-teams.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OurTeamCreateRequest $request)
    {
        $image = Input::file('img');
        $filename = time() . '.' . $image->getClientOriginalExtension();
        $path = public_path('images/' . $filename);

        Image::make($image->getRealPath())->save($path);

        $our_team = new Team;
        $our_team->title = $request->title;
        $our_team->img = $filename;

        $our_team->save();

        return redirect('admin/our-teams');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $our_team = Team::findOrFail($id);
        return view('backend.our-teams.edit',compact('our_team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OurTeamEditRequest $request, $id)
    {
        $image = Input::file('img');
        $our_team = Team::find($id);
        $our_team->title = $request->title;


        if ($image != null) {

            $file= $our_team->img;
            $old_file_name = public_path().'/images/'.$file;
            File::delete($old_file_name);

            $filename = time() . '.' . $image->getClientOriginalExtension();

            $path = public_path('images/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $our_team->img = $filename;
        }


        $our_team->save();

        return redirect('admin/our-teams');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $our_team = Team::findOrFail($id);
        $file= $our_team->img;
        $filename = public_path().'/images/'.$file;
        File::delete($filename);

        $our_team->delete();

        return redirect('/admin/our-teams');
    }
}
