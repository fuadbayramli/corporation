<?php

namespace App\Http\Controllers;

use App\Advantage;
use App\Alphabet;
use App\Article;
use App\News;
use App\OurClient;
use App\Portfolio;
use App\PortfolioCategory;
use App\Service;
use App\ServiceCategory;
use App\Team;
use App\Vocabulary;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service_cats = ServiceCategory::all();
        $portfolios = Portfolio::all();
        $advantages = Advantage::all();
        $all_news = News::orderBy('created_at','desc')->take(3)->get();
        $our_clients = OurClient::all();
        return view('frontend.index',compact('service_cats','portfolios','advantages','all_news','our_clients'));
    }



    public function portfolioCategory($cat_slug){

        $categories = PortfolioCategory::all();

        $category = PortfolioCategory::query()
            ->where('slug', $cat_slug)
            ->firstOrFail();

        $portfolio = Portfolio::query()
            ->where('category_id', $category->id)
            ->get();
        $portfolios = Portfolio::query()
            ->where('category_id', $category->id)
            ->orderBy('created_at', 'desc')
            ->paginate(50);

        return view('frontend.portfolios.category-details', compact('categories','category','portfolios','portfolio'));

    }

    public function portfolio($cat_slug,$port_slug){

        $prev = null;
        $next = null;

        $portfolio = Portfolio::where('slug',$port_slug)->firstOrFail();
        $category = PortfolioCategory::query()
            ->where('id', $portfolio->category_id)
            ->orderBy('created_at', 'desc')
            ->get();
        $portfolios = Portfolio::query()
            ->where('category_id', $portfolio->category_id)
            ->get();
        $categories = PortfolioCategory::all();

        foreach ($portfolios as $key => $p){

            if ($p->slug == $port_slug){
                if(($key+1) <= $portfolios->count() - 1 ){$next = ($portfolios[$key+1]) ;}
            }
        }

        foreach ($portfolios as $key => $p){
            if ($p->slug == $port_slug){
                if(($key-1) >= 0){$prev = $portfolios[$key-1];}
            }
        }

        return view('frontend.portfolios.portfolio-details', compact('portfolio','category','next','prev','categories'));
    }


    public function serviceCategory($slug){
        $categories = ServiceCategory::all();

        $category = ServiceCategory::query()
            ->where('slug', $slug)
            ->firstOrFail();

        $service = Service::query()
            ->where('category_id', $category->id)
            ->get();
        $services = Service::query()
            ->where('category_id', $category->id)
            ->orderBy('created_at', 'desc')
            ->paginate(50);

        return view('frontend.services.index', compact('categories','category','service','services'));
    }

    public function serv(){
        return view('frontend.services.all-services');
    }





    public function news(){
        $news = News::paginate(20);
        return view('frontend.news.index',compact('news'));
    }

    public function newsDetails($slug){
        $news = News::query()
            ->where('slug', $slug)
            ->firstOrFail();

        return view('frontend.news.news-details',compact('news'));
    }


    public function articles(){
        $articles = Article::paginate(20);
        return view('frontend.articles.index',compact('articles'));
    }


    public function articleDetails($slug){
        $article = Article::query()
            ->where('slug', $slug)
            ->firstOrFail();

        return view('frontend.articles.article-details',compact('article'));
    }




    public function team(){
        $teams = Team::paginate(12);
        return view('frontend.about.index',compact('teams'));
    }



    public function contact(){
        return view('frontend.contact.index');
    }


    public function vocabulary(){

        $alphabets = Alphabet::all();
        return view('frontend.alphabets.alphabet-details',compact('alphabets'));
    }

    public function alphabetDetails($slug){

        $alphabet_item = Alphabet::query()
            ->where('slug', $slug)
            ->firstOrFail();

        $vocabulary = Vocabulary::query()
            ->where('alphabet_id', $alphabet_item->id)
            ->orderBy('order_index', 'asc')
            ->first();

        $alphabets = Alphabet::all();

        return view('frontend.alphabets.alphabet-details', compact('alphabet_item', 'vocabulary','alphabets'));
    }














}











