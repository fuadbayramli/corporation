<?php

namespace App\Http\Controllers;

use App\Http\Requests\ServiceCreateRequest;
use App\Http\Requests\ServiceEditRequest;
use App\Service;
use App\ServiceCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class AdminServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $search_value = $request->get('q', '');
        $services = Service::query()
            ->where('name', 'LIKE', "%$search_value%")
            ->orWhere('slug', 'LIKE', "%$search_value%")
            ->orWhere('short', 'LIKE', "%$search_value%")
            ->orderBy('created_at', 'desc')
            ->paginate(50);
        return view('backend.services.index', compact( 'services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $service_categories = ServiceCategory::pluck('name', 'id')->all();

        return view('backend.services.create', compact('service_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceCreateRequest $request)
    {
        $image = Input::file('img');
        $filename = time() . '.' . $image->getClientOriginalExtension();
        $path = public_path('images/' . $filename);

        Image::make($image->getRealPath())->resize(790, 594)->save($path);

        $service = new Service;
        $service->img = $filename;
        $service->name = $request->name;
        $service->slug = $request->slug;
        $service->short = $request->short;
        $service->price = $request->price;
        $service->category_id = $request->category_id;

        $service->save();

        return redirect('admin/services');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::findOrFail($id);
        $service_categories = ServiceCategory::pluck('name','id')->all();

        return view('backend.services.edit',compact('service', 'service_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceEditRequest $request, $id)
    {
        $image = Input::file('img');

        $service = Service::find($id);
        $service->name = $request->name;
        $service->slug = $request->slug;
        $service->short = $request->short;
        $service->price = $request->price;
        $service->category()->associate($request->category_id);
        $service->save();

        if ($image != null) {

            $file= $service->img;
            $old_file_name = public_path().'/images/'.$file;
            File::delete($old_file_name);

            $filename = time() . '.' . $image->getClientOriginalExtension();

            $path = public_path('images/' . $filename);
            Image::make($image->getRealPath())->resize(790, 594)->save($path);
            $service->img = $filename;
        }

        return redirect('admin/services');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::findOrFail($id);
        $file = $service->img;
        $filename = public_path().'/images/'.$file;
        File::delete($filename);

        $service->delete();

        return redirect('/admin/services');
    }
}
