<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'title',
        'description',
        'category_id',
        'img',
    ];

    public function category(){
        return $this->belongsTo('App\PortfolioCategory');
    }
}
