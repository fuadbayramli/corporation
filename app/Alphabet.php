<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alphabet extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'lang',
    ];

    public function vocabularies(){
        return $this->hasMany('App\Vocabulary');
    }
}
