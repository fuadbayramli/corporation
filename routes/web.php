<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('/');

//Route::get('/post/{id}', ['as'=>'home.movie', 'uses'=>'AdminMovieController@post']);

Route::get('admin','AdminHomeController@index')->name('backend.index');


Route::resource('admin/roles', 'AdminRoleController', ['names'=>[

    'index'=>'backend.roles.index',
    'create'=>'backend.roles.create',
    'store'=>'backend.roles.store',
    'edit'=>'backend.roles.edit'

]]);

Route::resource('admin/users', 'AdminUserController', ['names'=>[

    'index'=>'backend.users.index',
    'create'=>'backend.users.create',
    'store'=>'backend.users.store',
    'edit'=>'backend.users.edit'

]]);

Route::resource('admin/role-items', 'AdminRoleItemController', ['names'=>[

    'index'=>'backend.role-items.index',

]]);


Route::resource('admin/portfolios', 'AdminPortfolioController', ['names'=>[

    'index'=>'backend.portfolios.index',
    'create'=>'backend.portfolios.create',
    'store'=>'backend.portfolios.store',
    'edit'=>'backend.portfolios.edit'

]]);

Route::resource('admin/advantages', 'AdminAdvantageController', ['names'=>[

    'index'=>'backend.advantages.index',
    'create'=>'backend.advantages.create',
    'store'=>'backend.advantages.store',
    'edit'=>'backend.advantages.edit'

]]);

Route::resource('admin/services', 'AdminServiceController', ['names'=>[

    'index'=>'backend.services.index',
    'create'=>'backend.services.create',
    'store'=>'backend.services.store',
    'edit'=>'backend.services.edit'

]]);


Route::resource('admin/site-information', 'AdminSiteInformationController', ['names'=>[

    'index'=>'backend.site-information.index',
    'create'=>'backend.site-information.create',
    'store'=>'backend.site-information.store',
    'edit'=>'backend.site-information.edit'

]]);


Route::resource('admin/news', 'AdminNewsController', ['names'=>[

    'index'=>'backend.news.index',
    'create'=>'backend.news.create',
    'store'=>'backend.news.store',
    'edit'=>'backend.news.edit'

]]);

Route::resource('admin/articles', 'AdminArticleController', ['names'=>[

    'index'=>'backend.articles.index',
    'create'=>'backend.articles.create',
    'store'=>'backend.articles.store',
    'edit'=>'backend.articles.edit'

]]);

Route::resource('admin/faqs', 'AdminFaqController', ['names'=>[

    'index'=>'backend.faqs.index',
    'create'=>'backend.faqs.create',
    'store'=>'backend.faqs.store',
    'edit'=>'backend.faqs.edit'

]]);
Route::resource('admin/cars', 'AdminCarController', ['names'=>[

    'index'=>'backend.cars.index',
    'create'=>'backend.cars.create',
    'store'=>'backend.cars.store',
    'edit'=>'backend.cars.edit'

]]);


Route::resource('admin/alphabets', 'AdminAlphabetController', ['names'=>[

    'index'=>'backend.alphabets.index',
    'create'=>'backend.alphabets.create',
    'store'=>'backend.alphabets.store',
    'edit'=>'backend.alphabets.edit'

]]);

Route::resource('admin/our-clients', 'AdminOurClientController', ['names'=>[

    'index'=>'backend.our-clients.index',
    'create'=>'backend.our-clients.create',
    'store'=>'backend.our-clients.store',
    'edit'=>'backend.our-clients.edit'

]]);
Route::resource('admin/our-teams', 'AdminTeamController', ['names'=>[

    'index'=>'backend.our-teams.index',
    'create'=>'backend.our-teams.create',
    'store'=>'backend.our-teams.store',
    'edit'=>'backend.our-teams.edit'

]]);
//
//Route::get('/search', 'HomeController@search');
//
//Route::get('/movies/{g_slug}', 'HomeController@generalMovies');
//
//Route::get('/other-movies/{o_slug}', 'HomeController@otherMovies');
//
//Route::get('/movie-details/{id}/{slug}', 'HomeController@movieDetails');
//
//Route::get('/movie-details/{id}/{slug}/{part_slug}', 'HomeController@moviePartDetails');

Route::get('/portfolio/{cat_slug}', 'HomeController@portfolioCategory');

Route::get('/portfolio/{cat_slug}/{port_slug}', 'HomeController@portfolio');

Route::get('/portfolio', 'HomeController@port');

Route::get('/service', 'HomeController@serv');


Route::get('/service/{slug}', 'HomeController@serviceCategory');

Route::get('/about/news', 'HomeController@news');

Route::get('/about/news/{slug}', 'HomeController@newsDetails');

Route::get('/about/articles/{slug}', 'HomeController@articleDetails');

Route::get('/about/articles', 'HomeController@articles');


Route::get('/about', 'HomeController@team');

Route::get('/contact', 'HomeController@contact');

Route::get('/about/vocabulary/', 'HomeController@vocabulary');

Route::get('/about/vocabulary/{slug}', 'HomeController@alphabetDetails');


//Route::get('/news-details/{id}/{slug}', 'HomeController@newsDetails');
//
//Route::get('/news', 'HomeController@news');
//
//Route::get('/privacy-policy', 'HomeController@privacyPolicy');
//
//
//
//
//
//
Route::get('admin-logout', '\App\Http\Controllers\Auth\LoginController@logout');
//
//
//
//
//
//
//
//Auth::routes();
//


Auth::routes();


