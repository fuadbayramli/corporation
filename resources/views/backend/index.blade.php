@extends('backend.layouts.app')
@section('content')

    <div class="right_col" role="main">
        <!-- top tiles -->
        <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <a href="{{ route('backend.users.index') }}">
                    <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
                    <div class="count">{{ $base_users->count() }}</div>
                </a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <a href="{{ route('backend.portfolios.index') }}">
                    <span class="count_top"><i class="fa fa-briefcase"></i> Total Portfolios</span>
                    <div class="count">{{ $base_portfolios->count() }}</div>
                </a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <a href="{{ route('backend.services.index') }}">
                    <span class="count_top"><i class="fa fa-tasks"></i> Total Services</span>
                    <div class="count">{{ $base_services->count() }}</div>
                </a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <a href="{{ route('backend.our-clients.index') }}">
                    <span class="count_top"><i class="fa fa-user-plus"></i> Total Clients</span>
                    <div class="count">{{ $base_clients->count() }}</div>
                </a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <a href="{{ route('backend.news.index') }}">
                    <span class="count_top"><i class="fa fa-newspaper-o"></i> Total News</span>
                    <div class="count">{{ $base_news->count() }}</div>
                </a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <a href="{{ route('backend.articles.index') }}">
                    <span class="count_top"><i class="fa fa-hacker-news"></i> Total Articles</span>
                    <div class="count">{{ $base_articles->count() }}</div>
                </a>
            </div>
        </div>
        <!-- /top tiles -->

    </div>




@endsection
