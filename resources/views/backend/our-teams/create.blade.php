@extends('backend.layouts.app')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left" style="margin-bottom: 13px">
                    <h3>Create Team</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            {!! Form::open(['method'=>'POST', 'class'=>'form-horizontal form-label-left', 'action'=> 'AdminTeamController@store', 'files'=>true]) !!}

                                <div class="item form-group">
                                    {!! Form::label('title', 'Title:', ['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {!! Form::text('title', null, ['class'=>'form-control col-md-7 col-xs-12'])!!}
                                        @if(!empty($errors->get('title')))
                                            <ul class="alert-danger">
                                                @foreach ($errors->get('title') as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </div>
                                </div>
                                <div class="item form-group">
                                    {!! Form::label('img', 'Image:', ['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {!! Form::file('img', null, ['class'=>'form-control col-md-7 col-xs-12'])!!}
                                        @if(!empty($errors->get('img')))
                                            <ul class="alert-danger">
                                                @foreach ($errors->get('img') as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        {!! Form::submit('Create', ['class'=>'btn btn-success', 'id' => 'send']) !!}
                                    </div>
                                </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">

        </div>
    </div>

@endsection

@section('customjs')
    <script src="{{ asset('backend/build/js/slugify.js') }}"></script>
    <script src="{{ asset('backend/ckeditor/ckeditor.js') }}"></script>


    <script>
        var post_name = $("#name");
        var post_slug_d = $("#slug");
        post_name.keyup(function () {
            str = post_name.val();
            post_slug_d.val(url_slug(str));

        });

    </script>
    <script>
        CKEDITOR.replace( 'editor1' );
    </script>

@endsection

