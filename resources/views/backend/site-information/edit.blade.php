@extends('backend.layouts.app')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left" style="margin-bottom: 13px">
                    <h3>Edit Site Information</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            {!! Form::model($site_information, ['method'=>'PATCH', 'class'=>'form-horizontal form-label-left', 'action'=> ['AdminSiteInformationController@update', $site_information->id], 'files'=>true]) !!}
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="thumbnail">
                                        <div class="image view view-first">
                                            <img style="width: 100%; display: block;" src="{{ asset("/images/$site_information->logo") }}" alt="image"/>
                                        </div>
                                        <div class="caption">
                                            <p>Header</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="thumbnail">
                                        <div class="image view view-first">
                                            <img style="width: 100%; display: block;" src="{{ asset("/images/$site_information->footer_logo") }}" alt="image"/>
                                        </div>
                                        <div class="caption">
                                            <p>Footer</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item form-group">
                                {!! Form::label('name', 'Name:', ['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::text('name', null, ['class'=>'form-control col-md-7 col-xs-12'])!!}
                                    @if(!empty($errors->get('name')))
                                        <ul class="alert-danger">
                                            @foreach ($errors->get('name') as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                            <div class="item form-group">
                                {!! Form::label('logo', 'Logo:', ['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::file('logo', null, ['class'=>'form-control col-md-7 col-xs-12'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                {!! Form::label('footer_logo', 'Footer Logo:', ['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::file('footer_logo', null, ['class'=>'form-control col-md-7 col-xs-12'])!!}
                                </div>
                            </div>
                            <div class="item form-group">
                                {!! Form::label('main_email', 'Eain Email:', ['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::email('main_email', null, ['class'=>'form-control col-md-7 col-xs-12'])!!}
                                    @if(!empty($errors->get('main_email')))
                                        <ul class="alert-danger">
                                            @foreach ($errors->get('main_email') as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                            <div class="item form-group">
                                {!! Form::label('main_number', 'Main Number:', ['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::text('main_number', null, ['class'=>'form-control col-md-7 col-xs-12'])!!}
                                    @if(!empty($errors->get('main_number')))
                                        <ul class="alert-danger">
                                            @foreach ($errors->get('main_number') as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                            <div class="item form-group">
                                {!! Form::label('address', 'Address:', ['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::text('address', null, ['class'=>'form-control col-md-7 col-xs-12'])!!}
                                    @if(!empty($errors->get('address')))
                                        <ul class="alert-danger">
                                            @foreach ($errors->get('address') as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                            <div class="item form-group">
                                {!! Form::label('correspond_address', 'Correspond Address:', ['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::text('correspond_address', null, ['class'=>'form-control col-md-7 col-xs-12'])!!}
                                    @if(!empty($errors->get('correspond_address')))
                                        <ul class="alert-danger">
                                            @foreach ($errors->get('correspond_address') as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                            <div class="item form-group">
                                {!! Form::label('link', 'Link:', ['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::text('link', null, ['class'=>'form-control col-md-7 col-xs-12'])!!}
                                    @if(!empty($errors->get('link')))
                                        <ul class="alert-danger">
                                            @foreach ($errors->get('link') as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                            <div class="item form-group">
                                {!! Form::label('short', 'Short:', ['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::textarea('short', null, ['class'=>'form-control col-md-7 col-xs-12', 'id' => 'editor1'])!!}
                                    @if(!empty($errors->get('short')))
                                        <ul class="alert-danger">
                                            @foreach ($errors->get('short') as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                            <div class="item form-group">
                                {!! Form::label('description', 'Description:', ['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::textarea('description', null, ['class'=>'form-control col-md-7 col-xs-12', 'id' => 'editor2'])!!}
                                    @if(!empty($errors->get('description')))
                                        <ul class="alert-danger">
                                            @foreach ($errors->get('description') as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                            <div class="item form-group">
                                {!! Form::label('status', 'Status:', ['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::select('status', ['Not Active' => 'Not Active', 'Active' => 'Active'], null, ['class'=>'form-control col-md-7 col-xs-12'])!!}
                                    @if(!empty($errors->get('status')))
                                        <ul class="alert-danger">
                                            @foreach ($errors->get('status') as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                    {!! Form::submit('Edit', ['class'=>'btn btn-success', 'id' => 'send']) !!}
                                </div>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="form-group">

        </div>

    </div>

@endsection


@section('customjs')
    <script src="{{ asset('backend/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace( 'editor1' );
    </script>
    <script>
        CKEDITOR.replace( 'editor2' );
    </script>

    <script>
        var post_name = $("#name");
        var post_slug_d = $("#slug");
        post_name.keyup(function () {
            str = post_name.val();
            post_slug_d.val(url_slug(str));

        });

    </script>


@endsection
