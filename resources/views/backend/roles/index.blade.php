@extends('backend.layouts.app')

@section('customcss')

    <link href="{{ asset('backend/css/sweet-alert.css') }}" rel="stylesheet">

@endsection

@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left" style="margin-bottom: 13px;">
                    <h3>Roles</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Permissions</th>
                                    <th>Created at</th>
                                    <th>Updated at</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($roles)
                                    @foreach($roles as $role)
                                        <tr>
                                            <td>{{$role->id}}</td>
                                            <td>{{$role->name}}</td>
                                            <td><a href="#">{{$role->slug}}</a></td>
                                            <td>
                                                {{ $role->role_items()->pluck('name')->implode(', ') }}
                                            </td>
                                            <td>{{$role->created_at->diffForhumans()}}</td>
                                            <td>{{$role->updated_at->diffForhumans()}}</td>
                                            <td><a href="{{ route('backend.roles.edit', $role->id) }}"><i class="fa fa-eye"></i></a>
                                                {!! Form::open(['method'=>'DELETE', 'action'=> ['AdminRoleController@destroy', $role->id],'id' => 'data-item-form'.$role->id]) !!}
                                                <div class="form-group">
                                                    <a href="javascript:void(0)" onclick="removeItem({{ $role->id }})"><i class="fa fa-trash"></i></a>
                                                </div>
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>

                                    @endforeach

                                @endif
                                </tbody>
                            </table>


                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-5">

                                    {{$roles->render()}}

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






@endsection

@section('customjs')

    <script src="{{ asset('backend/js/sweet-alert.js') }}"></script>
    <script>

        function removeItem($id) {

            swal({
                    title: "Are you sure",
                    text: "You will be able to recover this data!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, sure",
                    cancelButtonText: "No, cancel",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $('#data-item-form' + $id).submit();
                    } else {
                        swal("Cancelled", "You have cancelled", "error");
                    }
                });
        }
    </script>
@endsection

