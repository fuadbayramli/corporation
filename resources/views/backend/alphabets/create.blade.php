@extends('backend.layouts.app')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left" style="margin-bottom: 13px">
                    <h3>Create Alphabets</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            {!! Form::open(['method'=>'POST', 'class'=>'form-horizontal form-label-left', 'action'=> 'AdminAlphabetController@store', 'files'=>true]) !!}

                                <div class="item form-group">
                                    {!! Form::label('name', 'Name:', ['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {!! Form::text('name', null, ['class'=>'form-control col-md-7 col-xs-12'])!!}
                                        @if(!empty($errors->get('name')))
                                            <ul class="alert-danger">
                                                @foreach ($errors->get('name') as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </div>
                                </div>

                                <div class="item form-group">
                                    {!! Form::label('slug', 'Slug:', ['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {!! Form::text('slug', null, ['class'=>'form-control col-md-7 col-xs-12'])!!}
                                        @if(!empty($errors->get('slug')))
                                            <ul class="alert-danger">
                                                @foreach ($errors->get('slug') as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </div>
                                </div>
                                <div class="item form-group">
                                    {!! Form::label('lang', 'Language:', ['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {!! Form::select('lang', ['ingilis' => 'ingilis', 'rus' => 'rus'], null, ['class'=>'form-control col-md-7 col-xs-12'])!!}
                                        @if(!empty($errors->get('lang')))
                                            <ul class="alert-danger">
                                                @foreach ($errors->get('lang') as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </div>
                                </div>

                                <div class="input-group hdtuto control-group lst increment" >
                                    <div class="item form-group">

                                    </div>
                                    <div class="input-group-btn">
                                        <button class="btn btn-success" onclick="addNewRow()" id="btn-success" type="button"><i class="fldemo glyphicon glyphicon-plus"></i> Add Text</button>
                                    </div>
                                </div>

                                <div class="clone hide">
                                    <div class="hdtuto control-group lst input-group" style="margin-top:10px">
                                        <div class="item form-group">

                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                {!! Form::label('title', 'Vocabulary Name:', ['class'=>'control-label']) !!}
                                                {!! Form::text('title[]', null, ['class'=>'form-control'])!!}
                                            </div>
                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                {!! Form::label('short', 'Vocabulary Description:', ['class'=>'control-label']) !!}
                                                {!! Form::textarea('short[]', null, ['class'=>'form-control', 'rows' => 5])!!}
                                            </div>
                                            <div class="col-md-2 col-sm-2 col-xs-12">
                                                {!! Form::label('order_index', 'Order Index:', ['class'=>'control-label']) !!}
                                                {!! Form::text('order_index[]', null, ['class'=>'form-control'])!!}
                                            </div>
                                        </div>

                                        <div class="input-group-btn">
                                            <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        {!! Form::submit('Create', ['class'=>'btn btn-success', 'id' => 'send']) !!}
                                    </div>
                                </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">

        </div>
    </div>

@endsection

@section('customjs')
    <script src="{{ asset('backend/build/js/slugify.js') }}"></script>
    <script src="{{ asset('backend/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace( 'editor1' );
    </script>
    <script type="text/javascript">

            function addNewRow() {
                var lsthmtl = $(".clone").html();

                $(".increment").before(lsthmtl);
            }

            $("body").on("click",".btn-danger",function(){

                $(this).parents(".hdtuto.control-group.lst").remove();

            });
    </script>


    <script>
        var post_name = $("#name");
        var post_slug_d = $("#slug");
        post_name.keyup(function () {
            str = post_name.val();
            post_slug_d.val(url_slug(str));
//        }
        });

    </script>


@endsection

