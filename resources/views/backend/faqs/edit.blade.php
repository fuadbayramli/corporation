@extends('backend.layouts.app')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left" style="margin-bottom: 13px">
                    <h3>Edit Faq</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            {!! Form::model($faq, ['method'=>'PATCH', 'class'=>'form-horizontal form-label-left', 'action'=> ['AdminFaqController@update', $faq->id], 'files'=>true]) !!}
                                <div class="item form-group">
                                    {!! Form::label('question', 'Question:', ['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {!! Form::textarea('question', null, ['class'=>'form-control col-md-7 col-xs-12', 'id' => 'editor1'])!!}
                                        @if(!empty($errors->get('question')))
                                            <ul class="alert-danger">
                                                @foreach ($errors->get('question') as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </div>
                                </div>
                                <div class="item form-group">
                                    {!! Form::label('answer', 'Answer:', ['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {!! Form::textarea('answer', null, ['class'=>'form-control col-md-7 col-xs-12', 'id' => 'editor2'])!!}
                                        @if(!empty($errors->get('answer')))
                                            <ul class="alert-danger">
                                                @foreach ($errors->get('answer') as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </div>
                                </div>

                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        {!! Form::submit('Edit', ['class'=>'btn btn-success', 'id' => 'send']) !!}
                                    </div>
                                </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="form-group">

        </div>

    </div>

@endsection

@section('customjs')
    <script src="{{ asset('backend/build/js/slugify.js') }}"></script>
    <script src="{{ asset('backend/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace( 'editor1' );
    </script>
    <script>
        CKEDITOR.replace( 'editor2' );
    </script>

    <script>
        var post_name = $("#name");
        var post_slug_d = $("#slug");
        post_name.keyup(function () {
            str = post_name.val();
            post_slug_d.val(url_slug(str));

        });

    </script>


@endsection
