@extends('backend.layouts.app')

@section('customcss')
    <link href="{{ asset('backend/css/sweet-alert.css') }}" rel="stylesheet">

@endsection

@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Genres</h3>
                </div>

                <form class="title_right" method="get">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                        </div>
                    </div>
                </form>
            </div>

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Created at</th>
                                    <th>Updated at</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($role_items)
                                    @foreach($role_items as $role_item)
                                        <tr>
                                            <td>{{$role_item->id}}</td>
                                            <td>{{$role_item->name}}</td>
                                            <td><a href="#">{{$role_item->slug}}</a></td>
                                            <td>{{$role_item->created_at->diffForhumans()}}</td>
                                            <td>{{$role_item->updated_at->diffForhumans()}}</td>
                                        </tr>

                                    @endforeach

                                @endif
                                </tbody>
                            </table>


                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-5">

                                    {{$role_items->render()}}

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






@endsection

@section('customjs')
    <script src="{{ asset('backend/js/sweet-alert.js') }}"></script>
    <script>

        function removeItem($id) {

            swal({
                    title: "Are you sure",
                    text: "You will be able to recover this data!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, sure",
                    cancelButtonText: "No, cancel",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $('#data-item-form' + $id).submit();
                    } else {
                        swal("Cancelled", "You have cancelled", "error");
                    }
                });
        }
    </script>
@endsection

