<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href="{{asset('backend/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('backend/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">

@yield('customcss')

    <!-- Custom Theme Style -->
    <link href="{{asset('backend/build/css/custom.min.css')}}" rel="stylesheet">



</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{ url('/admin') }}" class="site_title"><i class="fa fa-building-o"></i> <span>Corporation</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="{{ asset('images')."/".Auth::user()->img }}" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2>{{ Auth::user()->name }}</h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            <li><a href="{{ route('backend.index') }}"><i class="fa fa-home"></i> Home </a>
                            </li>
                            <li><a><i class="fa fa-user"></i> Users <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ route('backend.users.index') }}">Users List</a></li>
                                    <li><a href="{{ route('backend.users.create') }}">Create User</a></li>
                                    <li><a href="{{ route('backend.roles.index') }}">Roles List</a></li>
                                    <li><a href="{{ route('backend.roles.create') }}">Create Role</a></li>
                                    <li><a href="{{ route('backend.role-items.index') }}">Role Items List</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-info-circle"></i> Site Information <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ route('backend.site-information.index') }}">Site Information List</a></li>
                                    <li><a href="{{ route('backend.site-information.create') }}">Create Site Information</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-briefcase"></i> Portfolios <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ route('backend.portfolios.index') }}">Portfolio List</a></li>
                                    <li><a href="{{ route('backend.portfolios.create') }}">Create Portfolio</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-tasks"></i> Services <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ route('backend.services.index') }}">Service List</a></li>
                                    <li><a href="{{ route('backend.services.create') }}">Create Service</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-info"></i>About <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ route('backend.news.index') }}">News List</a></li>
                                    <li><a href="{{ route('backend.news.create') }}">Create News</a></li>
                                    <li><a href="{{ route('backend.articles.index') }}">Articles List</a></li>
                                    <li><a href="{{ route('backend.articles.create') }}">Create Article</a></li>
                                    <li><a href="{{ route('backend.alphabets.index') }}">Alphabet List</a></li>
                                    <li><a href="{{ route('backend.alphabets.create') }}">Create Alphabet</a></li>
                                    <li><a href="{{ route('backend.our-teams.index') }}">Our Team List</a></li>
                                    <li><a href="{{ route('backend.our-teams.create') }}">Create Team</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-question-circle"></i>Faqs <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ route('backend.faqs.index') }}">Faq List</a></li>
                                    <li><a href="{{ route('backend.faqs.create') }}">Create Faq</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-buysellads"></i>Advantages <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ route('backend.advantages.index') }}">Advantage List</a></li>
                                    <li><a href="{{ route('backend.advantages.create') }}">Create Advantage</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-user-plus"></i>Our Clients <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ route('backend.our-clients.index') }}">Our Clients List</a></li>
                                    <li><a href="{{ route('backend.our-clients.create') }}">Create Client</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->

            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="{{ asset('images/')."/".Auth::user()->img }}" alt="">{{ Auth::user()->name }}
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="{{ url('admin-logout') }}">
                                        {{ __('Logout') }}</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        @yield('content')
        <!-- /page content -->

    </div>
</div>

<!-- jQuery -->
<script src="{{asset('backend/vendors/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('backend/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('backend/vendors/fastclick/lib/fastclick.js')}}"></script>
<!-- Custom Theme Scripts -->
<script src="{{asset('backend/build/js/custom.min.js')}}"></script>

<!-- Doughnut Chart -->
<script>
    $(document).ready(function(){
        var options = {
            legend: false,
            responsive: false
        };

        new Chart(document.getElementById("canvas1"), {
            type: 'doughnut',
            tooltipFillColor: "rgba(51, 51, 51, 0.55)",
            data: {
                labels: [
                    "Symbian",
                    "Blackberry",
                    "Other",
                    "Android",
                    "IOS"
                ],
                datasets: [{
                    data: [15, 20, 30, 10, 30],
                    backgroundColor: [
                        "#BDC3C7",
                        "#9B59B6",
                        "#E74C3C",
                        "#26B99A",
                        "#3498DB"
                    ],
                    hoverBackgroundColor: [
                        "#CFD4D8",
                        "#B370CF",
                        "#E95E4F",
                        "#36CAAB",
                        "#49A9EA"
                    ]
                }]
            },
            options: options
        });
    });
</script>
<!-- /Doughnut Chart -->

@yield('customjs')
</body>
</html>
