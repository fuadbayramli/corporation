<!DOCTYPE html>
<html>

<!-- Mirrored from www.ru-design.ru/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 16 Oct 2018 11:48:57 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <title>Создание сайтов — заказать сайт в "РУ ДИЗАЙН" | Разработка сайтов в Москве</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="robots" content="index, follow"/>
    <meta name="keywords"
          content="создание сайтов, создание сайтов москва, заказать создание сайта, создание сайтов под ключ, создание сайтов недорого, создание сайтов москва недорого, создание сайтов на заказ, изготовление сайта, разработка сайтов"/>
    <meta name="description"
          content="Создание сайтов в Москве - простота использования и поддержки. Хотите правильный продающий сайт? Закажите сайт в &quot;РУ ДИЗАЙН&quot; ☎ +7(495)971-12-57"/>
    <link href="{{ asset('frontend/local/templates/rd_2017/css/jquery.bxslider922a.css?14944314693431') }}" type="text/css"
          data-template-style="true" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}" type="text/css">
    <link href="{{ asset('frontend/local/templates/rd_2017/css/magnific-popup9fcc.css?14944314697412') }}" type="text/css"
          data-template-style="true" rel="stylesheet"/>
    <link href="{{ asset('frontend/local/components/rudes/rudes.callback/templates/.default/styled997.css?15009834332915') }}" type="text/css"
          data-template-style="true" rel="stylesheet"/>
    <link href="{{ asset('frontend/local/templates/rd_2017/components/bitrix/menu/top_menu/stylef4f8.css?14944314862397') }}" type="text/css"
          data-template-style="true" rel="stylesheet"/>
    <link href="{{ asset('frontend/local/templates/rd_2017/components/bitrix/breadcrumb/bc/style7e1e.css?1494431486620') }}" type="text/css"
          data-template-style="true" rel="stylesheet"/>
    <link href="{{ asset('frontend/local/templates/rd_2017/components/bitrix/system.pagenavigation/.default/styleef52.css?1494431485833') }}"
          type="text/css" data-template-style="true" rel="stylesheet"/>
    <link href="{{ asset('frontend/local/templates/rd_2017/components/bitrix/news.list/portfolio_slider/style6107.css?14944314831181') }}"
          type="text/css" data-template-style="true" rel="stylesheet"/>
    <link href="{{ asset('frontend/local/templates/rd_2017/components/bitrix/news.list/news_block/stylec7fb.css?1494431479612') }}"
          type="text/css" data-template-style="true" rel="stylesheet"/>
    <link href="{{ asset('frontend/local/templates/rd_2017/components/bitrix/news.list/clients_slider/style33d4.css?1494431484701') }}"
          type="text/css" data-template-style="true" rel="stylesheet"/>
    <link href="{{ asset('frontend/local/templates/rd_2017/components/bitrix/menu/footer_menu/style4d01.css?1494431486169') }}" type="text/css"
          data-template-style="true" rel="stylesheet"/>
    <link href="{{ asset('frontend/local/components/rudes/rudes.feedback/templates/.default/style413b.css?15010644583742') }}" type="text/css"
          data-template-style="true" rel="stylesheet"/>
    <link href="{{ asset('frontend/local/templates/rd_2017/stylesad2b.css') }}" type="text/css" data-template-style="true"
          rel="stylesheet"/>
    <link href="{{ asset('frontend/local/templates/rd_2017/template_stylesb6ab.css') }}" type="text/css"
          data-template-style="true" rel="stylesheet"/>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

    <!-- BEGIN JIVOSITE CODE -->
    <script type='text/javascript'>
        (function () {
            var widget_id = 'joZh1J2LUO';
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = '//code.jivosite.com/script/widget/' + widget_id;
            var ss = document.getElementsByTagName('script')[0];
            ss.parentNode.insertBefore(s, ss);
        })();
    </script>
    <!-- END JIVOSITE CODE -->


    <script type="text/javascript" src="{{ asset('frontend/local/templates/rd_2017/js/jquery.bxslider.minece2.js?149443146719186') }}"></script>
    <script type="text/javascript"
            src="{{ asset('frontend/local/templates/rd_2017/js/jquery.magnific-popup.min2da0.js?149443146717920') }}"></script>
    <script type="text/javascript"
            src="{{ asset('frontend/local/components/rudes/rudes.callback/templates/.default/scriptddb5.js?15214732271237') }}"></script>
    <script type="text/javascript"
            src="{{ asset('frontend/local/templates/rd_2017/components/bitrix/menu/top_menu/scriptd04f.js?1494431486469') }}"></script>
    <script type="text/javascript"
            src="{{ asset('frontend/local/components/rudes/rudes.feedback/templates/.default/script2eab.js?15214733002403') }}"></script>
    <script type="text/javascript">var _ba = _ba || [];
        _ba.push(["aid", "29a05791533bb024a33f9d9d73927bca"]);
        _ba.push(["host", "www.ru-design.ru"]);
        (function () {
            var ba = document.createElement("script");
            ba.type = "text/javascript";
            ba.async = true;
            ba.src = (document.location.protocol == "https:" ? "https://" : "http://") + "bitrix.info/ba.js";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(ba, s);
        })();</script>
    <script>new Image().src = 'http://ru-design.ru/bitrix/spread.php?s=QklUUklYX1NNX0dVRVNUX0lEATMyNTk1OAExNTcwNzk0NTM5AS8BAQECQklUUklYX1NNX0xBU1RfVklTSVQBMTYuMTAuMjAxOCAxNDo0ODo1OQExNTcwNzk0NTM5AS8BAQEC&amp;k=d2e87493c1c862bc8b2ce12f9a2f437a';
    </script>


    <script type="text/javascript">
        //<![CDATA[
        $(document).ready(function () {
            $('#content').magnificPopup({
                delegate: 'a.popup_img',
                type: 'image',
                tLoading: 'Loading image #%curr%...',
                mainClass: 'mfp-img-mobile',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
                },
                image: {
                    tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                    titleSrc: function (item) {
                        return item.el.attr('title');
                    }
                }
            });
        });
        //]]>
    </script>
    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>
<body>
<div id="header_bg">

    {{--@php dd($site_information);@endphp--}}
    <div id="header">
        <a href="{{ url('/') }}" id="header_logo" style="background: url({{ asset('images').'/'.$site_information->logo }}) no-repeat cover"></a>
        <div id="header_icons">
            <a href="javascript:void(0)" class="feedback-btn" data-role="rd-feedback" data-formtype="3"></a>
            <a href="{{ url('/contact') }}" class="map_icon"></a>
            <a href="sitemap/index.html" class="sitemap_icon"></a>				</div>
        <div id="header_phones">
            <a href="tel:+74959711257">{!! str_limit($site_information->main_number, 50) !!}</a>					<br />


            <div class="callback-btn-wrap">
                <a href="javascript:void(0)" id="callback-btn">ЗАКАЗАТЬ ОБРАТНЫЙ ЗВОНОК</a>
            </div>
            <div class="callback-bg" id="callback-block">
                <div class="callback-wrap">
                    <span id="callback-close">×</span>

                    <form id="rd-callback-form" action="http://www.ru-design.ru/" method="POST">
                        <input type="hidden" name="sessid" id="sessid" value="6c285fdc845d0a4186b2ed5344b50aba" />            <div class="input-item">
                            <label>Имя&nbsp;<span class="asterisk">*</span></label>
                            <input type="text" name="name" value="" required>
                        </div>
                        <div class="input-item">
                            <label>Контактный телефон&nbsp;<span class="asterisk">*</span></label>
                            <input type="tel" name="phone" value="" pattern="(\+?\d[- .]*){7,13}" title="+7 495 123-45-67" required>
                        </div>
                        <div class="input-item">
                            <label>Время звонка</label>
                            <select name="call-at-time">
                                <option value="fast">Не выбрано</option>
                                <option value="10:00">10:00</option>
                                <option value="11:00">11:00</option>
                                <option value="12:00">12:00</option>
                                <option value="13:00">13:00</option>
                                <option value="14:00">14:00</option>
                                <option value="15:00">15:00</option>
                                <option value="16:00">16:00</option>
                                <option value="17:00">17:00</option>
                                <option value="18:00">18:00</option>
                                <option value="19:00">19:00</option>
                            </select>
                        </div>
                        <div class="submit-item">
                            <input type="hidden" name="PARAMS_HASH" value="a437c4991743fa80c0abf1fd6c510a52">
                            <input type="submit" name="submit" value="ЗАКАЗАТЬ ЗВОНОК" onclick="yaCounter456011.reachGoal('call-back'); return true;">
                        </div>
                        <p class="pers-agree">Отправляя форму, Вы принимаете условия <a href="about/privacy-policy.html" target="_blank">Соглашения на обработку персональных данных</a></p>
                        <span class="preloader"></span>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="topmenu_bg">
    <div id="topmenu">

        <ul>


            <li><a href="{{ url('about') }}"><span>О компании</span></a>
                <ul>
                    <li><a href="{{ url('about/news') }}" class="item_1"><span>Новости</span></a></li>
                    <li><a href="{{ url('about/articles') }}" class="item_2"><span>Статьи</span></a></li>
                    <li><a href="about/vakansii/index.html" class="item_3"><span>Вакансии</span></a></li>
                    <li><a href="{{ url('about/vocabulary') }}" class="item_4"><span>Словарь интернет терминов</span></a></li>
                    <li><a href="about/blog/index.html" class="item_5"><span>Блог</span></a></li>



                </ul></li>
            <li><a href="{{ url('service') }}"><span>Услуги</span></a>
                <ul>
                    @foreach($base_serv_cats as $key => $serv_cat)
                        <li><a href="{{ url("/service/$serv_cat->slug") }}" class="item_{{$key+1}}">
                                <span>{{ $serv_cat->slug }}</span>
                            </a>
                        </li>
                    @endforeach
                </ul></li>
            <li><a href="{{ url("/portfolio/") }}"><span>Портфолио</span></a>
                <ul>

                    @foreach($base_port_cats as $key => $port_cat)
                        <li><a href="{{ url("/portfolio/$port_cat->slug") }}" class="item_{{$key+1}}">
                                <span>{{ $port_cat->name }}</span>
                            </a>
                        </li>
                    @endforeach
                        <li><a href="{{ url("/portfolio/") }}" class="item_5"><span>Все</span></a></li>

                    {{--<li><a href="portfolio/eshop/index.html" class="item_1"><span>Интернет-магазины</span></a></li>--}}
                    {{--<li><a href="portfolio/corporate/index.html" class="item_2"><span>Корпоративные сайты</span></a></li>--}}
                    {{--<li><a href="portfolio/inexpensive/index.html" class="item_3"><span>Недорогие сайты</span></a></li>--}}
                    {{--<li><a href="portfolio/complicated/index.html" class="item_4"><span>Порталы и сложные решения</span></a></li>--}}
                    {{--<li><a href="portfolio/index.html" class="item_5"><span>Все</span></a></li>--}}



                </ul></li>
            <li><a href="cases/index.html"><span>Кейсы</span></a></li>




            <li><a href="reviews/index.html"><span>Отзывы</span></a></li>




            <li><a href="{{ url('/contact')}}"><span>Контакты</span></a></li>





        </ul>
        <div class="menu-clear-left"></div>
        <div style="clear:both"></div>			</div>
</div>



@yield('content')



<div id="footer_bg">
    <div id="footer">
        <div id="footer_left">
            <a href="{{ url('/') }}" id="footer_logo"></a>
            <div class="contacts">
                <a href="tel:+74959711257">+7 (495) 971-12-57</a>
                <br />
                <a href="mailto:info@ru-design.ru" >info@ru-design.ru</a>
                <br />
                г. Москва, ул. Руставели д. 14 стр. 6					</div>
        </div>
        <div id="footer_center">
            <div class="footer_menu">
                <ul>
                    @foreach($base_serv_cats as $key => $serv_cat)
                        @if($key <= 3)
                            <li><a href="{{ url("service/$serv_cat->slug") }}">
                                    {{ $serv_cat->name }}
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
                <ul>

                    <li><a href="portfolio/index.html">Портфолио</a></li>


                    <li><a href="{{ url('about') }}">Компания</a></li>


                    <li><a href="reviews/index.html">Отзывы</a></li>


                    <li><a href="{{ url('contact') }}">Контакты</a></li>



                </ul>
                <div style="clear:both"></div>
            </div>
        </div>
        <div id="footer_right">
            <div id="copy">
                © 2000 — 2018 «РУ ДИЗАЙН» <br>
                Создание сайтов в Москве и реклама в интернете <br>					</div>
            <div id="social_groups">
                <!--a href="#" class="vk"></a-->
                <a href="#" class="gp"></a>
                <a href="https://www.facebook.com/rudesignmoscow" class="fb" target="_blank"></a>
                <a href="https://twitter.com/RuDesign2000" class="tw" target="_blank"></a>					</div>
        </div>
        <div style="clear:both"></div>
        <noindex>





            <div style="text-align: center">
                Оставляя свои личные данные, вы принимаете <a href="about/privacy-policy.html" target="_blank">Соглашение о конфиденциальности</a>.
            </div>
        </noindex>
    </div>
</div>


<div class="feedback-bg">
    <div class="feedback-hidden-block">
        <span class="feedback-close">x</span>
        <form class="rd-feedback-form" action="http://www.ru-design.ru/" method="POST">
            <div class="form-field">
                <label for="rd-feedback-name" class="name-field">Ваше имя</label>
                <label for="rd-feedback-name" class="helper-field">Введите ваше имя *</label>
                <input id="rd-feedback-name" type="text" class="input-item" name="name" required>
            </div>
            <div class="form-field">
                <label for="rd-feedback-phone" class="name-field">Ваш телефон</label>
                <label for="rd-feedback-phone" class="helper-field">Ваш телефон</label>
                <input id="rd-feedback-phone" type="tel" class="input-item" name="phone" pattern="(\+?\d[- .]*){7,13}" title="+7 495 123-45-67">
            </div>
            <div class="form-field">
                <label for="rd-feedback-email" class="name-field">Ваш e-mail</label>
                <label for="rd-feedback-email" class="helper-field">Введите ваш e-mail *</label>
                <input id="rd-feedback-email" type="email" class="input-item" name="email" required>
            </div>
            <div class="form-field">
                <label for="rd-feedback-info" class="name-field">Ваше сообщение</label>
                <label for="rd-feedback-info" class="helper-field">Введите сообщение *</label>
                <textarea id="rd-feedback-info" class="input-item" name="info" required></textarea>
            </div>
            <input id="feedback-form-source" type="hidden" name="source" value="">
            <input type="submit" name="submit" value="Отправить" onclick="yaCounter456011.reachGoal('feedback'); return true;">
            <p class="pers-agree">Отправляя форму, Вы принимаете условия <a href="about/privacy-policy.html" target="_blank">Соглашения на обработку персональных данных</a></p>
            <span class="preloader"></span>
        </form>
    </div>
</div>


@yield('js')


</body>

<!-- Mirrored from www.ru-design.ru/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 16 Oct 2018 11:49:46 GMT -->
</html>