@extends('frontend.layouts.app')
@section('content')

<div id="content_bg">
    <div class="bx_breadcrumbs"><ul><li><a href="../index.html" title="Главная">Главная</a></li><li><span>/</span></li><li><span>Контакты</span></li></ul></div>			<h1 id="pagetitle" class="header">Контакты</h1>
    <div id="content">
        <table cellpadding="10" cellspacing="0" style="border-collapse: collapse;" align="left">
            <tbody>
            <tr>
                <td>
                    <b>Наши телефоны:</b>
                </td>
                <td>
                    <a href="tel:+74959711257">{{ $site_information->main_number }}</a>
                </td>
            </tr>

            <tr>
                <td colspan="1">
                    <b>Наша электронная почта:</b>
                </td>
                <td colspan="1">
                    <a href="mailto:info@ru-design.ru">{{ $site_information->main_email }}</a>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <b>Адрес нашего офиса:</b>
                </td>
                <td colspan="1">
                    {{ $site_information->address }}
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <b>Адрес для корреспонденции:</b>
                </td>
                <td colspan="1">
                    127254 {{ $site_information->correspond_address }} <br>
                    ООО "РУ ДИЗАЙН"
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    {!! $site_information->short !!}
                </td>
            </tr>
            </tbody>
        </table>
        <div style="float: right">
            <script type="text/javascript">
                function BX_SetPlacemarks_MAP_mF8Ev4(map)
                {
                    if(typeof window["BX_YMapAddPlacemark"] != 'function')
                    {
                        /* If component's result was cached as html,
                         * script.js will not been loaded next time.
                         * let's do it manualy.
                        */

                        (function(d, s, id)
                        {
                            var js, bx_ym = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = "../bitrix/components/bitrix/map.yandex.view/templates/.default/script.js";
                            bx_ym.parentNode.insertBefore(js, bx_ym);
                        }(document, 'script', 'bx-ya-map-js'));

                        var ymWaitIntervalId = setInterval( function(){
                                if(typeof window["BX_YMapAddPlacemark"] == 'function')
                                {
                                    BX_SetPlacemarks_MAP_mF8Ev4(map);
                                    clearInterval(ymWaitIntervalId);
                                }
                            }, 300
                        );

                        return;
                    }

                    var arObjects = {PLACEMARKS:[],POLYLINES:[]};
                    arObjects.PLACEMARKS[arObjects.PLACEMARKS.length] = BX_YMapAddPlacemark(map, {'LON':'37.597246584323','LAT':'55.81321788252','TEXT':'РУ ДИЗАЙН\n+7 495 971-12-57\nул.Руставели д.14, стр.6'});
                }
            </script>
            <div class="bx-yandex-view-layout">
                <div class="bx-yandex-view-map">
                    <script>
                        var script = document.createElement('script');
                        script.src = 'http://api-maps.yandex.ru/2.0/?load=package.full&amp;mode=release&amp;lang=ru-RU&amp;wizard=bitrix';
                        (document.head || document.documentElement).appendChild(script);
                        script.onload = function () {
                            this.parentNode.removeChild(script);
                        };
                    </script>
                    <script type="text/javascript">

                        if (!window.GLOBAL_arMapObjects)
                            window.GLOBAL_arMapObjects = {};

                        function init_MAP_mF8Ev4()
                        {
                            if (!window.ymaps)
                                return;

                            if(typeof window.GLOBAL_arMapObjects['MAP_mF8Ev4'] !== "undefined")
                                return;

                            var node = BX("BX_YMAP_MAP_mF8Ev4");
                            node.innerHTML = '';

                            var map = window.GLOBAL_arMapObjects['MAP_mF8Ev4'] = new ymaps.Map(node, {
                                center: [55.813864294145, 37.598506730164],
                                zoom: 15,
                                type: 'yandex#map'
                            });

                            map.behaviors.enable("scrollZoom");
                            map.behaviors.enable("dblClickZoom");
                            map.behaviors.enable("drag");
                            if (map.behaviors.isEnabled("rightMouseButtonMagnifier"))
                                map.behaviors.disable("rightMouseButtonMagnifier");
                            map.controls.add('zoomControl');
                            map.controls.add('miniMap');
                            map.controls.add('typeSelector');
                            map.controls.add('scaleLine');
                            if (window.BX_SetPlacemarks_MAP_mF8Ev4)
                            {
                                window.BX_SetPlacemarks_MAP_mF8Ev4(map);
                            }
                        }

                        (function bx_ymaps_waiter(){
                            if(typeof ymaps !== 'undefined')
                                ymaps.ready(init_MAP_mF8Ev4);
                            else
                                setTimeout(bx_ymaps_waiter, 100);
                        })();


                        /* if map inits in hidden block (display:none)
                        *  after the block showed
                        *  for properly showing map this function must be called
                        */
                        function BXMapYandexAfterShow(mapId)
                        {
                            if(window.GLOBAL_arMapObjects[mapId] !== undefined)
                                window.GLOBAL_arMapObjects[mapId].container.fitToViewport();
                        }

                    </script>
                    <div id="BX_YMAP_MAP_mF8Ev4" class="bx-yandex-map" style="height: 440px; width: 320px;">загрузка карты...</div>	</div>
            </div>
        </div>
        <div style="clear: both;">
        </div>
        {!! $site_information->description !!}
    </div>

</div>
@endsection


@section('css')

    @endsection


@section('js')

    @endsection