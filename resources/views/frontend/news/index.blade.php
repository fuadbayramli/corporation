@extends('frontend.layouts.app')
@section('content')

<div id="content_bg">
    <div class="bx_breadcrumbs"><ul><li><a href="../../index.html" title="Главная">Главная</a></li><li><span>/</span></li><li><a href="../index.html" title="О компании">О компании</a></li><li><span>/</span></li><li><span>Новости</span></li></ul></div>			<h1 id="pagetitle" class="header">Новости</h1>
    <div id="content">
        <div class="news-list">
            @foreach($news as $key => $news_item)
                <p class="news-item" id="bx_3218110189_676">
                <span class="news-date-time">{{ $news_item->created_at->format('d.m.Y') }}</span>
                <a href="{{ url('about/news')."/".$news_item->slug }}"><b>{{ $news_item->name }}</b></a><br />
                {!! $news_item->short  !!} 									</p>
            @endforeach
            <br /><div class="navigation">

                <span class="nav-arrow-prev disabled">предыдущая страница</span>
                <span class="nav-current-page">1</span>
                <a href="index6a11.html?PAGEN_1=2">2</a>
                <a href="index2705.html?PAGEN_1=3">3</a>
                <a href="index7ad1.html?PAGEN_1=4">4</a>
                <a href="index6a11.html?PAGEN_1=2" class="nav-arrow-next" id="navigation_1_next_page">следующая страница</a>
                <div style="clear:both"></div>
            </div></div>

    </div>

</div>

@endsection

@section('css')

@endsection

@section('js')

@endsection