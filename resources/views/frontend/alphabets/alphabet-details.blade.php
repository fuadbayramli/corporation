@extends('frontend.layouts.app')
@section('content')

    <div id="content_bg">
        <div class="bx_breadcrumbs"><ul><li><a href="../../index.html" title="Главная">Главная</a></li><li><span>/</span></li><li><a href="../index.html" title="О компании">О компании</a></li><li><span>/</span></li><li><span>Словарь интернет терминов</span></li></ul></div>			<h1 id="pagetitle" class="header">Cловарь интернет терминов</h1>
        <div id="content">
            <script type="text/javascript">function goToChar(href){ location.href=href; }</script>
            <div style="border-bottom: 1px solid #ccc; padding-bottom: 3px;">
                <table cellpadding="0" cellspacing="6" border="0" align="center">
                    <tr>
                        @foreach($alphabets as $key => $alphabet)
                            @if($alphabet->lang == 'ingilis')
                                <td align="center" class="td_char" onClick="goToChar('1.html');">
                                    <a href="{{ url('about/vocabulary')."/".$alphabet->slug }}">{{ $alphabet->name }}</a>
                                </td>
                            @endif
                        @endforeach
                    </tr>
                </table>
            </div>
            <table cellpadding="0" cellspacing="6" border="0" align="center">
                <tr>
                    @foreach($alphabets as $key => $alphabet)
                        @if($alphabet->lang == 'rus')
                            <td align="center" class="td_char" onClick="goToChar('1.html');">
                                <a href="{{ url('about/vocabulary')."/".$alphabet->slug }}">{{ $alphabet->name }}</a>
                            </td>
                        @endif
                    @endforeach
                </tr>
            </table>
            <div style="margin-bottom: 30px;">&nbsp;</div>
            @if(isset($vocabulary))
                <ul>
                    @foreach($vocabulary->alphabet->vocabularies as $key => $vocabulary)
                        <li>{{$vocabulary->short}}</li>
                    @endforeach
                </ul>
            @else
                Данный раздел представляет из себя алфавитный указатель терминов, которые используются в сфере создания сайтов,
                их размещения в интернете, поискового продвижения сайтов и рекламы в интернете.
            @endif
        </div>

    </div>

                        {{--<ul>--}}
                            {{--@foreach($movie_item->parts as $key => $movie_part_item)--}}
                                {{--<li class="d-inline-block">--}}
                                    {{--<a href="{{ url("movie-details")."/".$movie_item->id."/".$movie_item->slug."/".$movie_part_item->embed_slug }}"--}}
                                       {{--class="theme-btn d-inline-block @if(isset($movie_part) && $movie_part_item->embed_slug == $movie_part->embed_slug) active @endif" style="margin: 10px 0 15px;">--}}
                                        {{--{{ $movie_part_item->embed_name }}--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--@endforeach--}}
                        {{--</ul>--}}
                        {{--@if(isset($movie_part))--}}

                            {{--{!! $movie_part->embed_code !!}--}}

                        {{--@endif--}}
                        {{--<div class="transformers-content">--}}
                            {{--<img src="{{ asset("/images/$movie_item->img") }}" alt="about"/>--}}
                        {{--</div>--}}

                    {{--<div class="col-lg-7">--}}
                        {{--<div class="transformers-content">--}}
                            {{--<p>--}}
                                {{--@foreach($movie_item->genres as $key => $movie_item_genre)--}}
                                    {{--{{ $movie_item_genre->name }}--}}
                                    {{--@if($key < count($movie_item->genres)-1)--}}
                                        {{--|--}}
                                    {{--@endif--}}
                                {{--@endforeach--}}
                            {{--</p>--}}
                            {{--<ul>--}}
                                {{--<li>--}}
                                    {{--<div class="transformers-left">--}}
                                        {{--Release:--}}
                                    {{--</div>--}}
                                    {{--<div class="transformers-right">--}}
                                        {{--{{ $movie_item->created_at->format('Y-m-d') }}--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<div class="transformers-left">--}}
                                        {{--Country:--}}
                                    {{--</div>--}}
                                    {{--<div class="transformers-right">--}}
                                        {{--{{ $movie_item->country }}--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}


@endsection

@section('css')

@endsection

@section('js')

@endsection