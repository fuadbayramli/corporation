@extends('frontend.layouts.app')
@section('content')

    <div id="content_bg">
        <div class="bx_breadcrumbs"><ul><li><a href="../../index.html" title="Главная">Главная</a></li><li><span>/</span></li><li><a href="../index.html" title="Услуги">Услуги</a></li><li><span>/</span></li><li><span>Создание сайтов</span></li></ul></div>
        <h1 id="pagetitle" class="header" style="background: url({{ asset('images')."/".$category->img }}) no-repeat top center; color: #fff;
                line-height: 137px;
                font-size: 42px;
                margin-top: 0;">{{ $category->name }}</h1>
        <div id="content">


            <div>
                {{ $category->short }}<div class="offers-list">

                    @foreach($services as $key => $service)
                        <div class="offer_item" id="bx_3218110189_103">
                            <div class="offer_header">
                                <img
                                        class="preview_picture"
                                        src="{{ asset('images')."/".$service->img }}"
                                        width="136"
                                        height="136"
                                        alt="РУ.Старт"
                                        title="РУ.Старт"
                                />
                                <span class="item_title">{{ $service->name }}</span>
                                <div style="clear:both"></div>
                            </div>
                            <div class="offer_text">
                                {!! $service->short !!} 							</div>
                            <div class="price_line">
                                <span class="price">от <span>{{ $service->price }}</span> рублей</span>
                                <a class="order_link" href="javascript:void(0)" data-role="rd-feedback" data-formtype="{{$key+5}}">заказать</a>
                            </div>
                        </div>
                        @if($key % 2 != 0)
                        <div style="clear:both"></div>
                        @endif
                    @endforeach
                    <br />	</div></div>
            <br>
        </div>

    </div>
    @endsection

@section('css')

    @endsection


@section('js')

    @endsection



