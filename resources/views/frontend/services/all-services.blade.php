@extends('frontend.layouts.app')
@section('content')


    <div id="content_bg">
        <div class="bx_breadcrumbs"><ul><li><a href="../index.html" title="Главная">Главная</a></li><li><span>/</span></li><li><span>Услуги</span></li></ul></div>			<h1 id="pagetitle" class="header">Услуги</h1>
        <div id="content">

            <p>Наша веб-студия занимается полным циклом работ в области интернет маркетинга. Мы готовы полностью взять на себя ответственность и добиться поставленного результата &ndash; разработать стратегию для вашего бизнеса с нуля до получения потока клиентов на ваш проект. <a href="../reviews/index.html" >Десятки историй успехов</a> наших клиентов с различными задачами и оборотами говорят за нашу студию лучше любых слов. Мы будем рады выполнить любую поставленную задачу с помощью самых современных инструментов доступных на рынке в кратчайший срок.</p>

        </div>

        <div id="service_block">
            @foreach($base_serv_cats as $key => $serv_cat)
                <div class="service_item @if($key == 0) create
                                        @elseif($key == 4) design
                                        @elseif($key == 5) mobile
                                        @else {{$serv_cat->slug}} @endif">
                    <div class="service_title">
                        <a href={{ url("service/$serv_cat->slug") }}><br>
                            {{ $serv_cat->name }}</a>
                    </div>
                    <div class="service_text">
                        {{ $serv_cat->short }}
                    </div>
                </div>
            @endforeach
            				<div style="clear:both"></div>
        </div>
    </div>

    @endsection

@section('css')

    @endsection

@section('js')

    @endsection