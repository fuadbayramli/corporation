@extends('frontend.layouts.app')
@section('content')

    <div id="content_bg">
    <div id="service_block">
        @foreach($service_cats as $key => $service_cat)
            <div class="service_item @if($key == 0) create
                                        @elseif($key == 4) design
                                        @elseif($key == 5) mobile
                                        @else {{$service_cat->slug}} @endif">
                <div class="service_title">
                    <a href={{ url("service/$service_cat->slug") }}><br>
                        {{ $service_cat->name }}</a>
                </div>
                <div class="service_text">
                    {{ $service_cat->short }}
                </div>
            </div>
        @endforeach
        <br>					<div style="clear:both"></div>
    </div>







    <h1 id="pagetitle" class="header">Создание сайтов для бизнеса под ключ</h1>
    <div id="content">
        <div style="font-size: 16px; line-height: 24px;">
            Правильный коммерческий сайт – это бизнес-инструмент, задача которого - умножать прибыль компании. Он виртуозно сочетает в себе приятный глазу визуал, достаточную для выбранной тематики функциональность, простоту использования и поддержки. &nbsp;Хотите правильный продающий сайт? Доверьте его создание команде «РУ ДИЗАЙН»! Для вас мы разработаем сайт любой сложности.&nbsp;
        </div>
        <div class="center-align" style="font-size: 16px; line-height: 24px;">
            <br>
        </div>
        <div class="center-align" style="font-size: 16px; line-height: 24px;">
            Наша специализация – создание сайтов на&nbsp;<a href="services/development/1c-bitrix.html">1С-Битрикс</a>&nbsp;и&nbsp;&nbsp;MODx&nbsp;
        </div>
        <div class="center-align" style="font-size: 16px; line-height: 24px;">
            <br>
        </div>
    </div>

</div>
<div id="portfolio_bg">
    <div id="portfolio_block">
        <div class="header">Портфолио проектов</div>
        <div id="porfolio_slider">
            @foreach($portfolios as $key => $portfolio)
                <div class="slide">
                    <a class="image_link" href="{{ url("portfolio")."/".$portfolio->category->slug."/".$portfolio->slug }}"><img src="{{ asset('images')."/".$portfolio->img }}" alt="Разработка сайта для загородного клуба" /></a>
                    <a class="slide_title" href="{{ url("portfolio")."/".$portfolio->category->slug."/".$portfolio->slug }}">{!! str_limit($portfolio->description, 80) !!}</a>
                </div>
            @endforeach
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#porfolio_slider').bxSlider({
                    slideWidth: 320,
                    minSlides: 1,
                    maxSlides: 3,
                    moveSlides: 1,
                    pager: false,
                    speed:1000,
                    pause:4000,
                    easing:'ease-in-out',
                    auto:false,
                    infiniteLoop:false,
                    hideControlOnEnd: true,
                    slideMargin: 5
                });
            });
        </script>
    </div>
</div>											<div id="triggers_bg">
    <div id="triggers_block">
        <div class="header">
            Почему выгодно работать с нами?
        </div>
        @foreach($advantages as $key => $advantage)
            <div class="trigger_item">
                <a href="#{{ $advantage->slug }}" class="trigger_icon {{ $advantage->slug }}"></a>
                <a href="#{{ $advantage->slug }}" class="trigger_title">{{ $advantage->name }}</a>
                <div class="trigger_text">
                    {{ $advantage->title }}
                </div>
            </div>
            <div class="mfp-hide white-popup-block zoom-anim-dialog" id="{{ $advantage->slug }}">
                {{ $advantage->short }}
            </div>
        @endforeach
        <script type="text/javascript">
            //<![CDATA[
            $(function () {
                $('.trigger_icon, .trigger_title').magnificPopup({
                    type: 'inline',
                    preloader: false,
                    modal: true,
                    fixedContentPos: false,
                    fixedBgPos: true,
                    overflowY: 'auto',
                    closeBtnInside: true,
                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'mfp-zoom-in'
                });

                $(document).on('click', '.popup-modal-dismiss', function (e) {
                    e.preventDefault();
                    $.magnificPopup.close();
                });
            });
            //]]>
        </script>					<div style="clear:both"></div>
    </div>
    <a href="javascript:void(0)" class="order_button" data-role="rd-feedback" data-formtype="4">Заказать сайт сейчас</a>
</div>
<div id="news_block_bg">
    <div id="news_block">
        @foreach($all_news as $key => $news)
            <div class="news-item">
                <div class="title">
                    <a href="about/news/uyazvimost-modx/index.html">{{ $news->name }}</a>
                    <span class="date">/ {{ $news->created_at->format('d.m.Y') }}</span>			</div>
                <div class="prev_text">
                    {!!  str_limit($news->short, 130) !!}				</div>
            </div>
        @endforeach
        <div style="clear:both"></div>
    </div>			</div>
<div id="clients_slider_bg">
    <div id="clients_slider_wrap">
        <div class="header">Наши клиенты</div>
        <div id="clients_slider">
            @foreach($our_clients as $key => $client)
                <div class="slide">
                    <img src="{{ asset('images')."/".$client->img }}" alt="Империо Групп" />
                </div>
            @endforeach
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#clients_slider').bxSlider({
                    slideWidth: 194,
                    minSlides: 1,
                    maxSlides: 5,
                    moveSlides: 0,
                    pager: false,
                    speed:3000,
                    pause:4000,
                    easing:'ease-in-out',
                    auto:true,
                    //responsive: true,
                    //adaptiveHeight: true,
                    slideMargin: 0
                });
            });
        </script>				</div>
</div>




@endsection

@section('css')

@endsection

@section('js')

@endsection
