@extends('frontend.layouts.app')
@section('content')
<div id="content_bg">
    <div class="bx_breadcrumbs"><ul><li><a href="../index.html" title="Главная">Главная</a></li><li><span>/</span></li><li><span>О компании</span></li></ul></div>			<h1 id="pagetitle" class="header">О компании</h1>
    <div id="content">
        <p>
            Наша компания существует на рынке интернет технологий уже более 15 лет. Замысел нашей компании - Помогать частным лицам и организациям продавать товары и услуги через Интернет.
        </p>
        <p>
            Нашими клиентами являются многие крупные организации из самых разных областей деятельности, от продажи сельскохозяйственной техники до банковского дела. Каждому клиенту гарантирован индивидуальный подход, с учётом специфики его деятельности Штат компании постоянно расширяется, сотрудники проходят обучение.
        </p>
        <p>
            Наша цель - предоставить клиенту максимально качественную услугу в минимальный срок.
        </p>
        <p>
            С отзывами о нашей работе вы можете ознакомиться в соответствующем <a href="../reviews/index.html" title="Отзывы">разделе</a>.
        </p>
        <div style="text-align: center;">
            @foreach($teams as $key => $team)
                <img width="130" alt="{{ $team->title }}" src="{{ asset('images')."/".$team->img }}" height="170" title="damir.jpg" hspace="5" vspace="10" border="0">
            @endforeach
        </div>
    </div>

</div>


@endsection

@section('css')

    @endsection

@section('js')

    @endsection