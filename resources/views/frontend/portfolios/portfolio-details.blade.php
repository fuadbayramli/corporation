@extends('frontend.layouts.app')
@section('content')

    <div id="content_bg">
    <div class="bx_breadcrumbs"><ul><li><a href="../../../index.html" title="Главная">Главная</a></li><li><span>/</span></li><li><a href="../../index.html" title="Портфолио">Портфолио</a></li><li><span>/</span></li><li><span>Корпоративные сайты</span></li></ul></div>			<h1 id="pagetitle" class="header">Компания «АДВ-Трейд» - создание сайта</h1>

    <div id="submenu_2">
        <ul>

            @php
                $active = -1;
                $after = -1;
                $before = -1;
            @endphp
            @foreach($categories as $key => $category)
                @if (Request::is("portfolio/$category->slug/"))
                    @php
                        $after = $key + 1;
                        $before = $key - 1;
                        $active = $key;
                    @endphp
                @endif

            @endforeach
            @foreach($categories as $key => $category)
                <li class="
                        @if($key == 0) first @elseif($key == 3 ) last @else middle @endif
                @if ($key == $active)
                        active
                @elseif($key == $after)
                                        after_active
                @elseif($key == $before)
                                        before_active
                @endif">
                    <a href={{ url("/portfolio/$category->slug") }}>{{ $category->name }}</a>
                </li>
            @endforeach








            {{--<li class="first before_active"><a href="../../eshop/index.html">Интернет-магазины</a></li>--}}


            {{--<li class="middle active"><a href="../index.html">Корпоративные сайты</a></li>--}}


            {{--<li class="middle after_active"><a href="../../inexpensive/index.html">Недорогие сайты</a></li>--}}


            {{--<li class="middle"><a href="../../complicated/index.html">Порталы и сложные решения</a></li>--}}


            {{--<li class="last"><a href="../../index.html">Все</a></li>--}}


            <div style="clear:both"></div>
        </ul>
    </div>
    <div id="content">
            <h2 class="work_title">{{ $portfolio->title }}</h2>
            <div class="work_detail">
                <div class="detail_text">{!! $portfolio->description !!}</div>

                <div class="detail_picture_bg">
                    <img class="detail_picture" border="0" src="{{ asset('images')."/".$portfolio->img }}"
                         alt="сайта" title="Компания «АДВ-Трейд» - создание сайта">
                </div>
                <div class="domain">{{ $portfolio->name }}</div>
                <div class="navigation">
                    @if($prev)
                        <a href="{{ url("/portfolio")."/".$prev->category->slug."/".$prev->slug }}" class="nav-arrow-prev">Предыдущая работа</a>
                    @else
                        <span class="nav-arrow-prev disabled">Предыдущая работа</span>
                    @endif
                    @if($next)
                            <a href="{{ url("/portfolio")."/".$next->category->slug."/".$next->slug }}" class="nav-arrow-next">Следующая работа</a>
                    @else
                        <span class="nav-arrow-next disabled">Следующая работа</span>
                    @endif
                    <div style="clear:both;"></div>
                </div>
            </div>

    </div>

</div>

@endsection

@section('css')

@endsection

@section('js')

@endsection