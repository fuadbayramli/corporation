@extends('frontend.layouts.app')
@section('content')

    <div id="content_bg">
        <div class="bx_breadcrumbs">
            <ul>
                <li><a href="../../index.html" title="Главная">Главная</a></li>
                <li><span>/</span></li>
                <li><a href="../index.html" title="Портфолио">Портфолио</a></li>
                <li><span>/</span></li>
                <li><span>Корпоративные сайты</span></li>
            </ul>
        </div>
        <h1 id="pagetitle" class="header">Портфолио</h1>

        <div id="submenu_2">
            <ul>
                @php
                    $active = -1;
                    $after = -1;
                    $before = -1;
                @endphp
                @foreach($categories as $key => $category)
                    @if (Request::is("portfolio/$category->slug"))
                        @php
                            $after = $key + 1;
                            $before = $key - 1;
                            $active = $key;
                        @endphp
                    @endif
                @endforeach

                @foreach($categories as $key => $category)
                    <li class="
                        @if($key == 0) first @elseif($key == 4) last @else middle @endif
                        @if ($key == $active)
                           active
                        @elseif($key == $after)
                            after_active
                        @elseif($key == $before)
                            before_active
                        @endif">
                        <a href={{ url("/portfolio/$category->slug") }}>{{ $category->name }}</a>
                    </li>
                @endforeach
                    <li class="last"><a href="../index.html">Все</a></li>


                {{--<li class="first before_active"><a href="">Интернет-магазины</a></li>--}}

                {{--<li class="middle active"><a href="{{ url("/portfolio-category")."/".$category->id."/".$category->slug }}">Корпоративные сайты</a></li>--}}


                {{--<li class="middle after_active"><a href="../inexpensive/index.html">Недорогие сайты</a></li>--}}


                {{--<li class="middle"><a href="../complicated/index.html">Порталы и сложные решения</a></li>--}}


                {{--<li class="last"><a href="../index.html">Все</a></li>--}}


                <div style="clear:both"></div>
            </ul>
        </div>
        <div id="content">
             
            <div class="work_list">
                @foreach($portfolios as $key => $portfolio)
                    <div class="work_item" id="bx_3218110189_447">
                        <div class="work_img">
                            <a href="{{ url('portfolio')."/".$portfolio->category->slug."/".$portfolio->slug }}">
                                <img class="preview_picture" border="0" src="{{ asset('images')."/".$portfolio->img }}"
                                     width="310" alt="клуба" title="Разработка сайта для загородного клуба" style="float:left"/>
                            </a>
                        </div>
                        <div class="work_info">
                            <a class="work_title" href="">{{ $portfolio->slug }}</a>
                            <div class="work_text">{!! str_limit($portfolio->short, 40) !!}</div>
                        </div>
                    </div>
                @endforeach
                <div style="clear:both"></div>
                <br/>
                <div class="navigation">

                    <span class="nav-arrow-prev disabled">предыдущая страница</span>
                    <span class="nav-current-page">1</span>
                    <a href="index5542.html?SECTION_CODE=corporate&amp;PAGEN_1=2">2</a>
                    <a href="index00c2.html?SECTION_CODE=corporate&amp;PAGEN_1=3">3</a>
                    ... <a href="indexb2cb.html?SECTION_CODE=corporate&amp;PAGEN_1=8">8</a>
                    <a href="indexe0f0.html?SECTION_CODE=corporate&amp;PAGEN_1=9">9</a>
                    <a class="nav-page-all" href="indexf0c1.html?SECTION_CODE=corporate&amp;SHOWALL_1=1">Все</a>
                    <a href="index5542.html?SECTION_CODE=corporate&amp;PAGEN_1=2" class="nav-arrow-next"
                       id="navigation_1_next_page">следующая страница</a>
                    <div style="clear:both"></div>
                </div>
            </div>

        </div>

    </div>



@endsection


@section('css')

@endsection

@section('js')

@endsection