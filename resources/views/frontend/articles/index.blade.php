@extends('frontend.layouts.app')
@section('content')

<div id="content_bg">
    <div class="bx_breadcrumbs"><ul><li><a href="../../index.html" title="Главная">Главная</a></li><li><span>/</span></li><li><a href="../index.html" title="О компании">О компании</a></li><li><span>/</span></li><li><span>Статьи</span></li></ul></div>			<h1 id="pagetitle" class="header">Статьи</h1>
    <div id="content">
        <div class="news-list">
            @foreach($articles as $key => $article)
                <p class="news-item" id="bx_3218110189_102">
                <a href="{{ url('about/articles')."/".$article->slug }}"><b>{{ $article->name }}</b></a><br />
                {{$article->title}}									</p>
            @endforeach
            <br /></div>

    </div>

</div>

@endsection

@section('css')

@endsection

@section('js')

@endsection