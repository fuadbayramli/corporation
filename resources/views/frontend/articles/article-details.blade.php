@extends('frontend.layouts.app')
@section('content')

    <div id="content_bg">
        <div class="bx_breadcrumbs"><ul><li><a href="../../index.html" title="Главная">Главная</a></li><li><span>/</span></li><li><a href="../index.html" title="О компании">О компании</a></li><li><span>/</span></li><li><span>Новости</span></li></ul></div>			<h1 id="pagetitle" class="header">Новости</h1>
        <div id="content">
            <div class="news-list">
                <p>{!! $article->short  !!}</p>
            </div>

        </div>

    </div>

@endsection

@section('css')

@endsection

@section('js')

@endsection